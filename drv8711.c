/*
 * drv8711.c
 *
 *  Created on: 2015 m�rc. 1
 *      Author: Grimgrin
 */

#include "drv8711.h"

static struct drv8711* m_config;
static uint8_t m_dir = 0;

static uint16_t __transfer(uint32_t base, uint16_t data) {
	uint32_t read;
	SSIDataPut(base, data);
	while (SSIBusy(base)) {

	}
	SSIDataGet(base, &read);
	return read;
}

static int write_register(uint16_t reg, uint16_t value) {
	uint16_t data = (reg << 12 | WRITE_OPERATION) | value;
	DRV8711_SELECT;
	__transfer(SPI_BASE, data);
	DRV8711_RELEASE;
	return 0;
}

static int read_register(uint16_t reg, uint16_t* value) {
	DRV8711_SELECT;
	uint16_t data = (reg << 12) | READ_OPERATION;
	*value = __transfer(SPI_BASE, data);
	*value &= 0x0FFF;
	DRV8711_RELEASE;
	return 0;
}

uint16_t drv8711_read_register(uint8_t reg) {
	uint16_t value;
	read_register(reg, &value);
	return value;
}

void drv8711_write_register(uint8_t reg, uint16_t value) {
	write_register(reg, value);
}

int drv8711_reset(void) {
	GPIOPinWrite(RESET_PORT_BASE, RESET_PORT_PIN, RESET_PORT_PIN);
	SysCtlDelay(50000);
	GPIOPinWrite(RESET_PORT_BASE, RESET_PORT_PIN, 0);
	return 0;
}

#ifdef EK_TM4C123GX_LAUNCHPAD

void drv8711_spi_init(uint32_t base) {
	uint32_t read;
	GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_6 | GPIO_PIN_7);
	GPIOPinConfigure(GPIO_PB4_SSI2CLK);
	GPIOPinConfigure(GPIO_PB6_SSI2RX);
	GPIOPinConfigure(GPIO_PB7_SSI2TX);
	SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
	SSI_MODE_MASTER, 100000, 16);
	SSIEnable(SSI2_BASE);
	while (SSIDataGetNonBlocking(SSI2_BASE, &read)) {
	}
}

#endif

int drv8711_init() {
	drv8711_spi_init(SPI_BASE);
	GPIOPinTypeGPIOOutput(NSLEEP_PORT_BASE, NSLEEP_PORT_PIN);
	DRV8711_ENABLE;
	SysCtlDelay(16000);
	GPIOPinTypeGPIOOutput(RESET_PORT_BASE, RESET_PORT_PIN);
	drv8711_reset();
	GPIOPinTypeGPIOOutput(CS_PORT_BASE, CS_PORT_PIN);
	DRV8711_RELEASE;
	GPIOPinTypeGPIOOutput(STEP_PORT_BASE, STEP_PORT_PIN);
	GPIOPinTypeGPIOOutput(DIR_PORT_BASE, DIR_PORT_PIN);

	GPIOPinTypeGPIOInput(NSTALL_PORT_BASE, NSTALL_PORT_PIN);
	GPIOPadConfigSet(NSTALL_PORT_BASE, NSTALL_PORT_PIN, GPIO_STRENGTH_4MA,
	GPIO_PIN_TYPE_STD_WPU);
	GPIOPinTypeGPIOInput(NFAULT_PORT_BASE, NFAULT_PORT_PIN);
	GPIOPadConfigSet(NFAULT_PORT_BASE, NFAULT_PORT_PIN, GPIO_STRENGTH_4MA,
	GPIO_PIN_TYPE_STD_WPU);

	SysCtlDelay(10000);
	DRV8711_ENABLE;
	return 0;
}

int drv8711_configure(struct drv8711* config) {
	m_config = config;
	write_register(DRV8711_REG_CTRL, m_config->ctrl);
	SysCtlDelay(100);
	write_register(DRV8711_REG_TORQUE, m_config->torque);
	SysCtlDelay(100);
	write_register(DRV8711_REG_OFF, m_config->off);
	SysCtlDelay(100);
	write_register(DRV8711_REG_BLANK, m_config->blank);
	SysCtlDelay(100);
	write_register(DRV8711_REG_DECAY, m_config->decay);
	SysCtlDelay(100);
	write_register(DRV8711_REG_STALL, m_config->stall);
	SysCtlDelay(100);
	write_register(DRV8711_REG_DRIVE, m_config->drive);
	SysCtlDelay(100);
	write_register(DRV8711_REG_STATUS, m_config->status);
	return 0;
}

int drv8711_read_config(struct drv8711* config) {
	read_register(DRV8711_REG_CTRL, &(config->ctrl));
	SysCtlDelay(100);
	read_register(DRV8711_REG_TORQUE, &config->torque);
	SysCtlDelay(100);
	read_register(DRV8711_REG_OFF, &config->off);
	SysCtlDelay(100);
	read_register(DRV8711_REG_BLANK, &config->blank);
	SysCtlDelay(100);
	read_register(DRV8711_REG_DECAY, &config->decay);
	SysCtlDelay(100);
	read_register(DRV8711_REG_STALL, &config->stall);
	SysCtlDelay(100);
	read_register(DRV8711_REG_DRIVE, &config->drive);
	SysCtlDelay(100);
	read_register(DRV8711_REG_STATUS, &config->status);
	return 0;
}

int drv8711_default_config(void) {
	//TODO: implement
	return 0;
}

uint16_t drv8711_get_status(void) {
	uint16_t value;
	read_register(0, &value);
	return value;
}

int drv8711_step(void) {
	GPIOPinWrite(STEP_PORT_BASE, STEP_PORT_PIN, STEP_PORT_PIN);
	SysCtlDelay(100);
	GPIOPinWrite(STEP_PORT_BASE, STEP_PORT_PIN, 0);
	return 0;
}

int drv8711_change_dir(void) {
	m_dir = ~m_dir;
	GPIOPinWrite(DIR_PORT_BASE, DIR_PORT_PIN, m_dir);
	return 0;
}

int drv8711_shutdown() {
	drv8711_disable_motor();
	DRV8711_SLEEP_MODE;
	return 0;
}

int drv8711_power_up(void) {
	DRV8711_ENABLE;
	return 0;
}

int drv8711_clear_fault(uint8_t mask) {
	write_register(DRV8711_REG_STATUS, mask);
	return 0;
}

int drv8711_disable_motor() {
	uint16_t value;
	read_register(DRV8711_REG_CTRL, &value);
	value &= ~ENABLE_MOTOR;
	write_register(DRV8711_REG_CTRL, value);
	return 0;
}

int drv8711_enable_motor() {
	uint16_t value;
	read_register(DRV8711_REG_CTRL, &value);
	value |= ENABLE_MOTOR;
	write_register(DRV8711_REG_CTRL, value);
	return 0;
}

int drv8711_increase_torque(uint8_t amount) {
	uint16_t value;
	uint16_t torque;
	read_register(DRV8711_REG_TORQUE, &value);
	torque = (value & TORQUE) + amount;
	if (torque > 255) {
		torque = 255;
	}
	value &= ~TORQUE;
	value |= torque;
	write_register(DRV8711_REG_TORQUE, value);
	return 0;
}

int drv8711_decrease_torque(uint8_t amount) {
	uint16_t value;
	uint16_t torque;
	read_register(DRV8711_REG_TORQUE, &value);
	torque = (value & TORQUE);
	if (torque > amount) {
		torque -= amount;
	} else {
		torque = 0;
	}
	value &= ~TORQUE;
	value |= torque;
	write_register(DRV8711_REG_TORQUE, value);
	return 0;
}

#ifdef UART_BUFFERED

void drv8711_dump(void) {
	struct drv8711 config;
	memset(&config, 0, sizeof(struct drv8711));
	drv8711_read_config(&config);
	UARTprintf("DRV8711 registers:\n");
	UARTprintf("CTRL:\t0x%X\nTORQUE:\t0x%X\nOFF:\t0x%X\nBLANK:\t0x%X\n",
			config.ctrl, config.torque, config.off, config.blank);
	UARTprintf("DECAY:\t0x%X\nSTALL:\t0x%X\nDRIVE:\t0x%X\nSTATUS:\t0x%X\n",
			config.decay, config.stall, config.drive, config.status);
}

#endif
