Driver library for DRV8711 based on TivaWare software library

Notes: (for hungarian please scroll down)

1. 	If you use this library on the EK-TM4C123GX launchpad, you need to define the following symbol:

	#define EK_TM4C123GX_LAUNCHPAD
	
	(Put this define directive to the very first line of main or set this symbol in the IDE you use.)
	If you do NOT use this library on the launchpad, you need to define the drv8711_spi_init(uint32_t base)
	function, and modify the drv8711_io.h header to match your setup.

Megjegyz�sek:

1.	A f�ggv�nyk�nyvt�r a TivaWare-n alapul, ez�rt sz�ks�ges be�llitani a megfelel� include path-okat, illetve a linker
	sz�m�ra sz�ks�ges megadni a leforditott libraryt. 

2. 	Ha a f�ggv�nyk�nyvt�rat az EK-TM4C123GX launchpadon haszn�lod, akkor defini�lnod kell a k�vetkez� szimb�lumot:

	#define EK_TM4C123GX_LAUNCHPAD
	
	(Rakd be ezt a sort a main elej�re, vagy defini�ld a szimb�lumot a fejleszt� k�rnyezetedben.)
	Ha NEM a launchpadot haszn�lod, akkor sz�ks�ges defini�lnod a drv8711_spi_init(uint32_t base) f�ggv�nyt,
	illetve m�dositsd megfelel�en a drv8711_io.h headert.
	
3. 	Ha defini�lva van az UART_BUFFERED szimb�lum, akkor haszn�lhat� a drv8711_dump() f�ggv�ny, amely kiolvassa a
	DRV8711 �sszes regiszter�t, majd a megfelel� UART portra form�zva kiiratja. 