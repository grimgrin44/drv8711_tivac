/*
 * drv8711.h
 *
 *  Created on: 2015 m�rc. 1
 *      Author: Grimgrin
 */

#ifndef DRV8711_DRV8711_H_
#define DRV8711_DRV8711_H_

#include "drv8711_defs.h"
#include "drv8711_io.h"
#include "../defs.h"
#include "../global_include.h"

struct drv8711 {
	uint16_t ctrl;
	uint16_t torque;
	uint16_t off;
	uint16_t blank;
	uint16_t decay;
	uint16_t stall;
	uint16_t drive;
	uint16_t status;
};

void drv8711_spi_init(uint32_t base);

int drv8711_init(void);
int drv8711_reset(void);
int drv8711_configure(struct drv8711* config);
int drv8711_default_config(void);
int drv8711_read_config(struct drv8711* config);
uint16_t drv8711_read_register(uint8_t reg);
void drv8711_write_register(uint8_t reg, uint16_t value);
uint16_t drv8711_get_status(void);
int drv8711_step(void);
int drv8711_change_dir(void);
int drv8711_shutdown(void);
int drv8711_power_up(void);
int drv8711_clear_fault(uint8_t mask);
int drv8711_disable_motor();
int drv8711_enable_motor();
int drv8711_decrease_torque(uint8_t amount);
int drv8711_increase_torque(uint8_t amount);

#ifdef UART_BUFFERED
void drv8711_dump(void);
#endif

#endif /* DRV8711_DRV8711_H_ */
