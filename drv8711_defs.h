/*
 * drv8711_defs.h
 *
 *  Created on: 2015 febr. 28
 *      Author: Grimgrin
 */

#ifndef DRV8711_DRV8711_DEFS_H_
#define DRV8711_DRV8711_DEFS_H_

/* Register addresses */

#define DRV8711_REG_CTRL		0x00
#define DRV8711_REG_TORQUE		0x01
#define DRV8711_REG_OFF			0x02
#define DRV8711_REG_BLANK		0x03
#define DRV8711_REG_DECAY		0x04
#define DRV8711_REG_STALL		0x05
#define DRV8711_REG_DRIVE		0x06
#define DRV8711_REG_STATUS		0x07

/* Bit field masks definitions */

/* CTRL register bit masks */
#define DTIME					(0x03 << 10)
#define ISGAIN					(0x03 << 8)
#define EXSTALL 				(0x01 << 7)
#define MODE					(0x0F << 3)
#define RSTEP					(0x01 << 2)
#define RDIR					(0x01 << 1)
#define ENBL					(0x01 << 0)

/* Torque register bit masks */
#define SMPLTH					(0x07 << 8)
#define TORQUE					(0xFF << 0)

/* Off register bit masks */
#define PWMMODE					(0x01 << 8)
#define TOFF					(0xFF << 0)

/* Blank register bit masks */
#define ABT						(0x01 << 8)
#define TBLANK					(0xFF << 0)

/* Decay register bit masks */
#define DECMOD					(0x07 << 8)
#define TDECAY					(0xFF << 0)

/* Stall register bit masks */
#define VDIV					(0x03 << 10)
#define SDCNT					(0x03 << 10)
#define SDTHR					(0xFF << 0)

/* Driver register bit masks */
#define IDRIVEP					(0x03 << 10)
#define IDRIVEN					(0x03 << 8)
#define TDRIVEP					(0x03 << 6)
#define TDRIVEN					(0x03 << 4)
#define OCPDEG					(0x03 << 2)
#define OCPTH					(0x03 << 0)

/* Status register bit masks */
#define STDLAT					(0x01 << 7)
#define STD						(0x01 << 6)
#define UVLO					(0x01 << 5)
#define BPDF					(0x01 << 4)
#define APDF					(0x01 << 3)
#define BOCP					(0x01 << 2)
#define AOCP					(0x01 << 1)
#define OTS						(0x01 << 0)


/* CTRL register bits */
#define ENABLE_MOTOR			(0x01 << 0)
#define DISABLE_MOTOR			(0x00 << 0)
#define DIR_SET_BY_DIR_PIN		(0x01 << 1)
#define	DIR_SET_BY_INV_DIR_PIN	(0x00 << 1)
#define STEP_ONE				(0x01 << 2)
#define MODE_FULL_STEP			(0x00 << 3)
#define MODE_HALF_STEP			(0x01 << 3)
#define MODE_1_4_STEP			(0x02 << 3)
#define MODE_1_8_STEP			(0x03 << 3)
#define MODE_1_16_STEP			(0x04 << 3)
#define MODE_1_32_STEP			(0x05 << 3)
#define MODE_1_64_STEP			(0x06 << 3)
#define MODE_1_128_STEP			(0x07 << 3)
#define MODE_1_256_STEP			(0x08 << 3)
#define INTERNAL_STALL_DETECT	(0x01 << 7)
#define EXTERNAL_STALL_DETECT	(0x00 << 7)
#define ISENSE_GAIN_5			(0x00 << 8)
#define ISENSE_GAIN_10			(0x01 << 8)
#define ISENSE_GAIN_20			(0x02 << 8)
#define ISENSE_GAIN_40			(0x03 << 8)
#define DTIME_400ns				(0x00 << 10)
#define DTIME_450ns				(0x01 << 10)
#define DTIME_650ns				(0x02 << 10)
#define DTIME_850ns				(0x03 << 10)

/* Torque register */
#define SMPLTH_50us				(0x00 << 8)
#define SMPLTH_100us			(0x01 << 8)
#define SMPLTH_200us			(0x02 << 8)
#define SMPLTH_300us			(0x03 << 8)
#define SMPLTH_400us			(0x04 << 8)
#define SMPLTH_600us			(0x05 << 8)
#define SMPLTH_800us			(0x06 << 8)
#define SMPLTH_1000us			(0x07 << 8)

/* Off register */
#define PWM_MODE_INTERNAL_IDX	(0x01 << 8)
#define PWM_MODE_BYPASS_IDX		(0x00 << 8)

/* Blank register */
#define ABT_ENABLED				(0x01 << 8)
#define ABT_DISABLED			(0x00 << 8)

/* Decay register */
#define DECMOD_FORCE_SLOW		(0x00 << 8)
#define DECMOD_SLOW_INC_MIX_DEC	(0x01 << 8)
#define DECMOD_FORCE_FAST		(0x02 << 8)
#define DECMOD_MIXED			(0x03 << 8)
#define DECMOD_SLOW_INC_AUTO_MIX_DEC	(0x04 << 8)
#define DECMOD_AUTO_MIXED		(0x05 << 8)

/* Stall register */
#define SDCNT_STALLn_ASSERT_1_STEP	(0x00 << 8)
#define SDCNT_STALLn_ASSERT_2_STEP	(0x01 << 8)
#define SDCNT_STALLn_ASSERT_4_STEP	(0x02 << 8)
#define SDCNT_STALLn_ASSERT_8_STEP	(0x03 << 8)
#define VDIV_32						(0x00 << 10)
#define VIDV_16						(0x01 << 10)
#define VDIV_8						(0x02 << 10)
#define VDIV_4						(0x03 << 10)

/* Drive register */
#define OCPTH_250mV					(0x00 << 0)
#define OCPTH_500mV					(0x01 << 0)
#define OCPTH_750mV					(0x02 << 0)
#define OCPTH_1000mV				(0x03 << 0)
#define OCPDEG_1us					(0x00 << 2)
#define OCPDEG_2us					(0x01 << 2)
#define OCPDEG_4us					(0x02 << 2)
#define OCPDEG_8us					(0x03 << 2)
#define TDRIVEN_250ns				(0x00 << 4)
#define TDRIVEN_500ns				(0x01 << 4)
#define TDRIVEN_1us					(0x02 << 4)
#define TDRIVEN_2us					(0x03 << 4)
#define TDRIVEP_250ns				(0x00 << 6)
#define TDRIVEP_500ns				(0x01 << 6)
#define TDRIVEP_1us					(0x02 << 6)
#define TDRIVEP_2us					(0x03 << 6)
#define IDRIVEN_100mA				(0x00 << 8)
#define IDRIVEN_200mA				(0x01 << 8)
#define IDRIVEN_300mA				(0x02 << 8)
#define IDRIVEN_400mA				(0x03 << 8)
#define IDRIVEP_50mA				(0x00 << 10)
#define IDRIVEP_100mA				(0x01 << 10)
#define IDRIVEP_150mA				(0x02 << 10)
#define IDRIVEP_200mA				(0x03 << 10)

/* Status register */
#define NORMAL_OPERATION			(0x00)
#define OVERTEMPERATURE_SHUTDOWN	(0x01 << 0)
#define CHANNEL_A_OVERCURRENT_SHUTDOWN	(0x01 << 1)
#define CHANNEL_B_OVERCURRENT_SHUTDOWN	(0x01 << 2)
#define CHANNEL_A_PREDRIVER_FAULT	(0x01 << 3)
#define CHANNEL_B_PREDRIVER_FAULT	(0x01 << 4)
#define UNDERVOLTAGE_LOCKOUT		(0x01 << 5)
#define STALL_DETECTED				(0x01 << 6)
#define LATCHED_STALL_DETECTED		(0x01 << 7)

#define WRITE_OPERATION				(0x0000)
#define READ_OPERATION				(0x8000)















#endif /* DRV8711_DRV8711_DEFS_H_ */
